package service;

import java.util.Set;

import model.FavoriteList;

public interface FavoriteListService
{
  Set<FavoriteList> getAll();

  FavoriteList save(FavoriteList favoriteList);

  FavoriteList get(Long listId);
}

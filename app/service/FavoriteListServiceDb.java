package service;

import java.util.HashSet;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Singleton;

import model.FavoriteList;
import play.db.jpa.JPAApi;

@Singleton
public class FavoriteListServiceDb implements FavoriteListService
{
  private final JPAApi jpaApi;

  @Inject
  public FavoriteListServiceDb(JPAApi jpaApi)
  {
    this.jpaApi = jpaApi;
  }

  @Override
  public Set<FavoriteList> getAll()
  {
    return new HashSet<>(jpaApi.em().createQuery("select l from FavoriteList l").getResultList());
  }

  @Override
  public FavoriteList get(Long listId)
  {
    return jpaApi.em().find(FavoriteList.class, listId);
  }

  @Override
  public FavoriteList save(FavoriteList favoriteList)
  {
    return jpaApi.em().merge(favoriteList);
  }
}

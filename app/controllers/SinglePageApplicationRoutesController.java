package controllers;

import javax.inject.Inject;

import play.Environment;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * A hack to allow usage of single page application with any url starts with "app".
 * Please look to conf/routs file for more information
 */
public class SinglePageApplicationRoutesController extends Controller
{
  private Environment environment;

  @Inject
  public SinglePageApplicationRoutesController(Environment environment)
  {
    this.environment = environment;
  }

  public Result index(String somePath)
  {
    return ok(this.environment.getFile("/public/index.html"), true);
  }
}

package controllers;

import javax.inject.Inject;

import model.FavoriteList;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import service.FavoriteListService;

public class FavoriteListController extends Controller
{
  private FavoriteListService favoriteListService;

  @Inject
  public FavoriteListController(FavoriteListService favoriteListService)
  {
    this.favoriteListService = favoriteListService;
  }

  @Transactional(readOnly = true)
  public Result get(Long listId)
  {
    return ok(Json.toJson(favoriteListService.get(listId)));
  }

  @Transactional(readOnly = true)
  public Result getAll()
  {
    return ok(Json.toJson(favoriteListService.getAll()));
  }

  @Transactional
  public Result save()
  {
    FavoriteList favoriteList = Json.fromJson(request().body().asJson(), FavoriteList.class);
    return ok(Json.toJson(favoriteListService.save(favoriteList)));
  }
}

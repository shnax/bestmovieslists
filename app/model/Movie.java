package model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "MOVIE")
public class Movie
{
  @Id
  private Long id;
  private String title;
  @JsonProperty("poster_path")
  private String thumbnail;

  public Movie()
  {
  }

  public Movie(long id, String title, String thumbnail)
  {
    this.id = id;
    this.title = title;
    this.thumbnail = thumbnail;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    Movie movie = (Movie) o;

    return id.equals(movie.id);
  }

  @Override
  public int hashCode()
  {
    return id.hashCode();
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public String getThumbnail()
  {
    return thumbnail;
  }

  public void setThumbnail(String thumbnail)
  {
    this.thumbnail = thumbnail;
  }
}

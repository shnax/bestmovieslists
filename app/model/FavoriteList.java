package model;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "FAVORITE_LIST")
public class FavoriteList
{
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  @Column(unique = true)
  private String name;

  @ManyToMany(cascade = CascadeType.ALL)
  @JoinTable(
      name = "FAVORITE_LIST_NO_MOVIE",
      joinColumns = @JoinColumn(name = "FAVORITE_LIST_ID"),
      inverseJoinColumns = @JoinColumn(name = "MOVIE_ID"))
  private Set<Movie> movies;

  public FavoriteList()
  {
  }

  public FavoriteList(Long id, String name, Set<Movie> movies)
  {
    this.id = id;
    this.name = name;
    this.movies = movies;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    FavoriteList that = (FavoriteList) o;

    if (id != null ? !id.equals(that.id) : that.id != null)
      return false;
    return name.equals(that.name);
  }

  @Override
  public int hashCode()
  {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + name.hashCode();
    return result;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public Set<Movie> getMovies()
  {
    return movies;
  }

  public void setMovies(Set<Movie> movies)
  {
    this.movies = movies;
  }
}

import {Injectable} from "@angular/core";
import {Jsonp, Response} from "@angular/http";
import {Observable} from "rxjs/Rx";
import {Movie} from "./model.movie";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

@Injectable()
export class ThemoviedbService {
    constructor(private jsonp: Jsonp) {
    }

    searchMovies(searchQuery: string): Observable<Movie[]> {
        const API_KEY: string = '7a4de0fe5da237bdb52d1168dae8cd14';
        const searchUrl: string = `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&language=en-US&query=${searchQuery}&page=1&include_adult=false&callback=JSONP_CALLBACK`;

        return this.jsonp.get(searchUrl)
            .map((response: Response) => response.json().results)
    }
}

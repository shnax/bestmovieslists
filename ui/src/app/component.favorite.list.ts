import {FavoriteListService} from "./service.favorite.list";
import {FavoriteList} from "./model.favorite.list";
import {OnInit, Component} from "@angular/core";
import {ActivatedRoute} from "@angular/router";

@Component({
    templateUrl: './component.favorite.list.html',
})
export class FavoriteListComponent implements OnInit {
    private favoriteList: FavoriteList;

    constructor(private favoriteListService: FavoriteListService, private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        const listId: number = this.route.snapshot.params['listId'];
        this.favoriteListService.get(listId).subscribe(list => this.favoriteList = list);
    }
}
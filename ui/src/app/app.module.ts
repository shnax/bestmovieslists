import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule, JsonpModule} from "@angular/http";
import {ThemoviedbService} from "./service.themoviedb";
import {FavoriteListService} from "./service.favorite.list";
import {RouterModule} from "@angular/router";
import {RoutesComponent} from "./app.routes";
import {APP_BASE_HREF} from "@angular/common";
import {AppComponent} from "./app.component";
import {WelcomeComponent} from "./component.welcome";
import {FavoriteListComponent} from "./component.favorite.list";

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    FavoriteListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    RouterModule.forRoot(RoutesComponent)
  ],
  providers: [ThemoviedbService, FavoriteListService, {provide: APP_BASE_HREF, useValue: "/"}],
  bootstrap: [AppComponent]
})
export class AppModule {
}

import {Routes} from "@angular/router";
import {WelcomeComponent} from "./component.welcome";
import {FavoriteListComponent} from "./component.favorite.list";

export const RoutesComponent: Routes = [
    {path: '', component: WelcomeComponent},
    {path: 'app/favorite/list/:listId', component: FavoriteListComponent}
];
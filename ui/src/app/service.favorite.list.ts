import {FavoriteList} from "./model.favorite.list";
import {Movie} from "./model.movie";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";

@Injectable()
export class FavoriteListService {
    private lists: FavoriteList[] = [];

    constructor(private http: Http) {
    }

    getAll(): Observable<FavoriteList[]> {
        return this.http.request('/favorite/lists').map(res => res.json())
    }

    get(listId: number): Observable<FavoriteList> {
        return this.http.request(`/favorite/list/${listId}`).map(res => res.json())
    }

    createFavoriteList(listName: string): Observable<FavoriteList> {
        return this.saveFavoriteList(new FavoriteList(null, listName, []));
    }

    addMovieToSelectedFavoriteLists(movie: Movie, favoriteLists: FavoriteList[]) {
        favoriteLists
            .filter(list => list.isSelected && this.isNotMoviePresent(movie, list))
            .forEach(list => this.saveMovieToFavoriteList(list, movie));
    }

    private isNotMoviePresent(movie: Movie, list: FavoriteList): boolean {
        return !list.movies
            .map(movie => movie.id)
            .some(movieId => movieId === movie.id)
    }

    private saveMovieToFavoriteList(list: FavoriteList, movie: Movie) {
        list.movies.push(movie)
        this.saveFavoriteList(list).subscribe(savedList => console.log(savedList));
    }

    private saveFavoriteList(favoriteList: FavoriteList): Observable<FavoriteList> {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(favoriteList);

        return this.http.post('/favorite/list', body, options)
            .map((res: Response) => res.json());
    }
}
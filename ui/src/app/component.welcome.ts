import {Component, OnInit} from "@angular/core";
import {ThemoviedbService} from "./service.themoviedb";
import {FavoriteListService} from "./service.favorite.list";
import {Movie} from "./model.movie";
import {FavoriteList} from "./model.favorite.list";

@Component({
    templateUrl: './component.welcome.html',
    styleUrls: ['./component.welcome.css']
})
export class WelcomeComponent implements OnInit {
    private movies: Movie[];
    private favoriteLists: FavoriteList[];

    constructor(private themoviedbService: ThemoviedbService, private favoriteListService: FavoriteListService) {
    }

    ngOnInit(): void {
        this.favoriteListService.getAll().subscribe(lists => this.favoriteLists = lists);
    }

    searchMovies(searchQuery: string): void {
        this.themoviedbService.searchMovies(searchQuery).subscribe(movies => this.movies = movies);
    }

    addFavoriteList(listName: string): void {
        if (!this.favoriteLists.some(list => list.name === listName)) {
            this.favoriteListService.createFavoriteList(listName)
                .subscribe(createdList => this.favoriteLists.push(createdList));
        }
    }

    addMovieToFavoriteLists(movie: Movie): void {
        this.favoriteListService.addMovieToSelectedFavoriteLists(movie, this.favoriteLists)
    }

    selectList(favoriteList: FavoriteList): void {
        this.favoriteLists.forEach(list => list.isSelected = false);
        favoriteList.isSelected = true;
    }
}

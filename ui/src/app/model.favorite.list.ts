import {Movie} from "./model.movie";

export class FavoriteList {
    public isSelected: boolean = false;

    constructor(public id: number,
                public name: string,
                public movies: Movie[]) {
    }
}
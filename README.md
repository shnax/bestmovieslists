## Build and run:

To build UI files please run next commands under "ui" folder: 

`npm install`

`ng build`

And then run next command under root folder to start application:

`sbt run`

## Functionality overview:
![BestMoviesWelcomePage.png](https://bitbucket.org/repo/kMqe86a/images/1115824394-BestMoviesWelcomePage.png)

![UniqueLinkToShare.png](https://bitbucket.org/repo/kMqe86a/images/3946911100-UniqueLinkToShare.png)